#include <stdio.h>
#include <stdlib.h>
#include "ttable.h"

int main(int argc, char const *argv[]) {

	for (int i = 1; i < argc; ++i) {
		printf("Arg: %s\n", argv[i]);
	}

	if (argc == 3) {
		unsigned int noOfVars;
		sscanf(argv[1], "%2u", &noOfVars);
		char vars[noOfVars];
		parse(noOfVars, vars, argv[2]);
	} else {
		printf("Invalid arguments supplied, exiting.");
		exit(1);
	}
	return 0;
}

int parse(int noOfVars, char n[], char input[]) {
	int finished = 0;
	while (!finished) {
		char current;
		sscanf(input, "%c", &current); // Look for variable
		if ((current >= 97) && (current <= (97 + noOfVars - 1))) {
			parse(noOfVars, n, input);
		} else {
			return 0;
		} 
	}
	return 0;
}